//
//  swiftACMacros.swift
//  CWYouJi
//
//  Created by MC on 15/11/14.
//  Copyright © 2015年 MC. All rights reserved.
//

import Foundation
import UIKit
//#define AppTextCOLOR      RGBCOLOR(127, 125, 147);

var AppTextCOLOR : UIColor = RGBA (r: 127, g: 125, b: 147)
var AppCOLOR : UIColor = kUIColorFromRGB(0x40aefc)
var AppMCBgCOLOR : UIColor = kUIColorFromRGB(0xf8f8f8)
var AppMCNACOLOR : UIColor = kUIColorFromRGB(0xf6f6f6)

func RGBA (r:CGFloat, g:CGFloat, b:CGFloat) ->UIColor {
    
 return UIColor.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1)
    
    
}
//tW切图w   VW限定的w 返回对应的h

func MCAdaptiveH (TW :CGFloat ,TH : CGFloat,VW :CGFloat) ->CGFloat{
    
   return (((VW) * (TH)) / (TW))
    
    
}
func MCAdaptiveW (TW :CGFloat ,TH : CGFloat,VH :CGFloat) ->CGFloat{
    
    return  (((TW) *  (VH)) / (TH))
    
    
}
func MCHeightScale() -> CGFloat{
    
  return  Main_Screen_Height / 568.0000

}





func kUIColorFromRGB (_ rgbValue:Int) -> UIColor{
    
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0, green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0, blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0, alpha: 1.0)

    //  Converted with Swiftify v1.0.6285 - https://objectivec2swift.com/
//return UIColor(red: rgbValue/255, green: rgbValue/255, blue: rgbValue/255, alpha: 1)
    
    
    
}

func ViewBorderRadius (_ View:UIView , Radius:CGFloat , Width : CGFloat ,Color:UIColor){
    View.layer.cornerRadius = Radius
    View.layer.masksToBounds = true
    View.layer.borderWidth = Width
    View.layer.borderColor = Color.cgColor
    
    
}
func ViewRadius(_ View : UIView,Radius : CGFloat){
    View.layer.cornerRadius = Radius
    View.layer.masksToBounds = true

    
}



let  version = (UIDevice.current.systemVersion as NSString).floatValue

// 屏幕
let Main_Screen_Height = UIScreen.main.bounds.height
// 屏幕
let Main_Screen_Width = UIScreen.main.bounds.width
// 默认图片
let defaultImg = UIImage(named: "photo_define")
// NSUserDefault
let userDefault = UserDefaults.standard
// 通知中心
let notice = NotificationCenter.default
//判断是不是plus
let currentModeSize = UIScreen.main.currentMode?.size
////字符串转数组
//func stringToArray(Str str:String)->NSArray{
//    var dataArray:[String] = []
//    for items in str{
//        dataArray.append("\(items)")
//    }
//    return dataArray
//}
