//
//  ViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/22.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let root = MainTableViewController()
        let appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
        appDelegate?.window?.rootViewController = root
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

