//
//  MCMApManager.swift
//  MCSwift
//
//  Created by MC on 2017/3/23.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class MCMApManager: NSObject ,AMapLocationManagerDelegate {
    
    let DefaultLocationTimeout = 10
    
    let DefaultReGeocodeTimeout = 5

    var _ctlView : BaseViewController!
    
    var locationManager : AMapLocationManager!
    var completionBlock: AMapLocatingCompletionBlock!
    var lo : Double!
    var la : Double!
    var city : String!
    
    
    
    
    static var sharedInstance:MCMApManager{
        
        
        struct MyStatic{
            
            static var instance : MCMApManager = MCMApManager()
            
        }
       MyStatic.instance.locationManager = AMapLocationManager()

        return MyStatic.instance;
    }

    
    func updateLocation(){
        
        self.locationManager.delegate = self
        
        //设置期望定位精度

        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        //设置不允许系统暂停定位
        self.locationManager.pausesLocationUpdatesAutomatically = false
        //设置允许在后台定位
        self.locationManager.allowsBackgroundLocationUpdates = true
        
        
        //设置定位超时时间
        self.locationManager.locationTimeout = DefaultLocationTimeout
        //设置逆地理超时时间
        self.locationManager.reGeocodeTimeout = DefaultReGeocodeTimeout
        initCompleteBlock()
        reGeocodeAction()
        locAction()

    }
    func reGeocodeAction(){
        //进行单次带逆地理定位请求
        locationManager.requestLocation(withReGeocode: true, completionBlock: completionBlock)
        
        
    }
    func locAction(){
        //进行单次定位请求

        locationManager.requestLocation(withReGeocode: false, completionBlock: completionBlock)
 
    }
    
    func initCompleteBlock(){
        completionBlock = { [weak self] (location: CLLocation?, regeocode: AMapLocationReGeocode?, error: Error?) in
            if let error = error {
                let error = error as NSError
                
                if error.code == AMapLocationErrorCode.locateFailed.rawValue {
                    //定位错误：此时location和regeocode没有返回值，不进行annotation的添加
                    NSLog("定位错误:{\(error.code) - \(error.localizedDescription)};")
                    return
                }
                else if error.code == AMapLocationErrorCode.reGeocodeFailed.rawValue
                    || error.code == AMapLocationErrorCode.timeOut.rawValue
                    || error.code == AMapLocationErrorCode.cannotFindHost.rawValue
                    || error.code == AMapLocationErrorCode.badURL.rawValue
                    || error.code == AMapLocationErrorCode.notConnectedToInternet.rawValue
                    || error.code == AMapLocationErrorCode.cannotConnectToHost.rawValue {
                    
                    //逆地理错误：在带逆地理的单次定位中，逆地理过程可能发生错误，此时location有返回值，regeocode无返回值，进行annotation的添加
                    NSLog("逆地理错误:{\(error.code) - \(error.localizedDescription)};")
                }
                else {
                    //没有错误：location有返回值，regeocode是否有返回值取决于是否进行逆地理操作，进行annotation的添加
                }
            }
            
            //修改label显示内容
            if let location = location {
                
                
                
                
                if let regeocode = regeocode {
                    
                    let responseString1: String = regeocode.city.replacingOccurrences(of: "市", with: "")

                    
                    self?.city = responseString1
                    
                    
                }
                else {
                    
                    
                 self?.la =  location.coordinate.latitude
                    self?.lo = location.coordinate.longitude
                    

                    
                }
            }
            
        }

        
        
        
    }
    
    

}
