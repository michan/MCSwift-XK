//
//  AlertViewExtension.swift
//  MCSwift
//
//  Created by MC on 2017/3/27.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

protocol alertviewExtensionDelegate: NSObjectProtocol {
    func clickBtnSelector(_ btn: UIButton)
}

class AlertViewExtension: UIView {

    
    var _cancelBtn : UIButton!
    var _sureBtn : UIButton!
    var _tipeLabel : UILabel!
    var _tipebackView : UIView!
    weak var delegate: alertviewExtensionDelegate?

    override init(frame: CGRect) {
        
      super.init(frame: frame)
        //设置模板层背景色
        self.backgroundColor = UIColor(red: CGFloat(129 / 255.0), green: CGFloat(129 / 255.0), blue: CGFloat(129 / 255.0), alpha: CGFloat(0.7))
        _tipebackView = UIView(frame: CGRect(x: CGFloat(30), y: CGFloat((frame.size.height - 150) / 2), width: CGFloat(frame.size.width - 40), height: CGFloat(150)))
        _tipebackView.backgroundColor = UIColor.white
        _tipebackView.layer.cornerRadius = 5
        self.addSubview(_tipebackView)
        _tipeLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(10), width: CGFloat(_tipebackView.frame.size.width - 20), height: CGFloat(_tipebackView.frame.size.height - 20)))
        _tipeLabel.textAlignment = .center
        _tipeLabel.numberOfLines = 0
        _tipebackView.addSubview(_tipeLabel)
        
        
        _cancelBtn = UIButton(type: .custom)
        _cancelBtn.tag = 2000
        _cancelBtn.setTitle("取消", for: .normal)
        _cancelBtn.setTitleColor(UIColor(red: CGFloat(154 / 255.0), green: CGFloat(154 / 255.0), blue: CGFloat(154 / 255.0), alpha: CGFloat(1)), for: .normal)
        
        if #available(iOS 8.2, *) {
            _cancelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: 0.5)
        } else {
            _cancelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
            // Fallback on earlier versions
        }
        
        
        _cancelBtn.addTarget(self, action: #selector(self.btnClickSelector), for: .touchUpInside)
        _cancelBtn.backgroundColor = UIColor(red: CGFloat(247 / 255.0), green: CGFloat(247 / 255.0), blue: CGFloat(247 / 255.0), alpha: CGFloat(1.0))
        ViewBorderRadius(_cancelBtn, Radius: 0, Width: 0.5, Color: UIColor.groupTableViewBackground)
        _tipebackView.addSubview(_cancelBtn)
        
        _sureBtn = UIButton(type: .custom)
        _sureBtn.tag = 1000
        _sureBtn.setTitle("确定", for: .normal)
        _sureBtn.setTitleColor(UIColor.white, for: .normal)
        if #available(iOS 8.2, *) {
            _sureBtn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(16), weight: CGFloat(0.5))
        } else {
            _sureBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
            // Fallback on earlier versions
        }
        _sureBtn.addTarget(self, action: #selector(self.btnClickSelector), for: .touchUpInside)
        _sureBtn.backgroundColor = UIColor(red: CGFloat(254 / 255.0), green: CGFloat(72 / 255.0), blue: CGFloat(68 / 255.0), alpha: CGFloat(1.0))
        ViewBorderRadius(_sureBtn, Radius: 0, Width: 0.5, Color: UIColor.groupTableViewBackground)
        _tipebackView.addSubview(_sureBtn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setbackviewframeWidth(_ width: CGFloat, andheight height: CGFloat) {
        let tipeheight: CGFloat = (frame.size.height - height) / 2
        let tipewidth: CGFloat = (frame.size.width - width) / 2
        _tipebackView.frame = CGRect(x: tipewidth, y: tipeheight, width: width, height: height)
        _tipeLabel.frame = CGRect(x: CGFloat(10), y: CGFloat(5), width: CGFloat(_tipebackView.frame.size.width - 20), height: CGFloat(_tipebackView.frame.size.height - 55))
        _cancelBtn.frame = CGRect(x: CGFloat(0), y: CGFloat(_tipebackView.frame.size.height - 45), width: CGFloat(width / 2), height: CGFloat(45))
        _sureBtn.frame = CGRect(x: CGFloat(width / 2), y: CGFloat(_tipebackView.frame.size.height - 45), width: CGFloat(width / 2), height: CGFloat(45))
    }
    
    
    //设置提示语
    
    func settipeTitleStr(_ tipeStr: String, andfont font: CGFloat) {
        _tipeLabel.font = UIFont.systemFont(ofSize: font)
        _tipeLabel.text = tipeStr
    }
    func btnClickSelector(_ btn: UIButton) {
        
        
        
        if delegate != nil {
            delegate?.clickBtnSelector(btn)
        }
    }
    
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
