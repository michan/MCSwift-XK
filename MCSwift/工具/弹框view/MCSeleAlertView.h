//
//  MCSeleAlertView.h
//  MCSchoolTaxi
//
//  Created by MC on 2017/2/28.
//  Copyright © 2017年 MC. All rights reserved.
//
#define AppCOLOR      kUIColorFromRGB(0x40aefc)//主蓝色
#define kUIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]



#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width
// View 圆角
#define ViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]
// View 圆角和加边框
#define ViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]


// View 圆角和加边框
#define ViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]





#import <UIKit/UIKit.h>


@protocol MCSeleAlertViewDelegate <NSObject>
@required

-(void)MCSeleAlertViewHidden;

@optional

-(void)MCSeleAlertView:(NSInteger)selectIndex;

-(void)MCSeleAlertView:(NSInteger)selectDateIndex Index:(NSInteger)index;



@end




@interface MCSeleAlertView : UIView
@property(nonatomic,weak)id<MCSeleAlertViewDelegate>deldagate;

- (void)showInWindow;
-(instancetype)initWithFrame:(CGRect)frame MCstyle:(NSInteger)index;


-(instancetype)initWithFrame:(CGRect)frame  MCDateTyle:(NSInteger)dateindex MCstyle:(NSInteger)index;

- (void)showAnimateInWindow;






@end
