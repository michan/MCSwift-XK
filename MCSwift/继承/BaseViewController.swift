//
//  BaseViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/22.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit


var _appColor = kUIColorFromRGB(0x40aefc)
var _appMCBgCOLOR  = kUIColorFromRGB(0xf8f8f8)
var _appMCNACOLOR  = kUIColorFromRGB(0xf6f6f6)


class BaseViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = _appMCBgCOLOR

        
    self.automaticallyAdjustsScrollViewInsets = false
        
        appColorNavigation()
        
        // Do any additional setup after loading the view.
    }
    
    func appColorNavigation(){
        
        let titleColor = UIColor.darkText
        //  Converted with Swiftify v1.0.6285 - https://objectivec2swift.com/
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : titleColor,
            NSFontAttributeName : UIFont(name: "CourierNewPSMT", size: CGFloat(18.0))]
        
        self.navigationController?.navigationBar.barTintColor = _appMCNACOLOR
        self.navigationController?.navigationBar.tintColor = _appColor
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-999999999, -999999999), for: .default)
        
        
    }
    
    
    func pushNewViewController(_ newViewController:UIViewController){
        
       self.navigationController?.pushViewController(newViewController, animated: true)
        
    }
    func toPopVC(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    func stopshowLoading() {
        SVProgressHUD.dismiss()
    }
    
    func showLoading(_ title: String) {
        if (title.characters.count > 0) {
            SVProgressHUD.show(withStatus: title)
        }
        else {
            SVProgressHUD.show()
        }
    }
    
    func showAllTextDialog(_ title: String) {
        if (title.characters.count > 0) {
            SVProgressHUD.show(nil, status: title)
        }
        else {
            
            SVProgressHUD.showError(withStatus: "操作失败")
        }
    }
    func showLoading(){
        SVProgressHUD.show()
    }
    func showSuccess(){
        SVProgressHUD.showSuccess(withStatus: "操作成功")
        
    }
    func showError(){
        SVProgressHUD.showError(withStatus: "操作失败")

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
