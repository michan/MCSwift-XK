//
//  MainTableViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/22.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class MainTableViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    func setupViews(){
       let home = MyTaxicabViewController()
        
        self.setUpChildController(home, title: "我要租车", image: "tab_bike", selectedImage: "tab_bike_pre", tag: 9000)
        
        let Organiview = MyOwnerViewController()
        
        self.setUpChildController(Organiview, title: "我是车主", image: "tab_key", selectedImage: "tab_key_pre", tag: 9001)
        

        let meview = MeViewController()
        
        self.setUpChildController(meview, title: "我的", image: "tab_me", selectedImage: "tab_me_pre", tag: 9002)
        
        
        
    }
    func setUpChildController(_ controller:UIViewController,  title:String ,image:String,selectedImage:String ,tag:NSInteger){
        
        controller.title = title as String
        self.tabBar.tintColor = _appColor
        //默认的图片
        controller.tabBarItem.image = UIImage(named: image as String)?.withRenderingMode(.alwaysOriginal)
        
        
        // if (title)
        //选择时的图片,将其渲染方式设置为原始的颜色
        controller.tabBarItem.selectedImage = UIImage(named: selectedImage as String)?.withRenderingMode(.alwaysOriginal)
        controller.tabBarItem.tag = tag
        let nav = MCNavViewController(rootViewController: controller)
        self.addChildViewController(nav)
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
