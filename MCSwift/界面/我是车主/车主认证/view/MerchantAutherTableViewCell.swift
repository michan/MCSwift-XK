//
//  MerchantAutherTableViewCell.swift
//  MCSwift
//
//  Created by MC on 2017/3/27.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class MerchantAutherTableViewCell: UITableViewCell {
    weak var viewController: MerchantAutherViewController!
    var seleAddBtn: UIButton?
    var textArray = [Any]()

    func prepareUI(){
        for view: UIView in contentView.subviews {
            view.removeFromSuperview()
        }
        contentView.backgroundColor = AppMCBgCOLOR
        var x: CGFloat = 10
        var y: CGFloat = 0
        var w: CGFloat = Main_Screen_Width - 2 * x
        var h: CGFloat = 44 * 5 + 0.5 * 4
        let bgView = UIView(frame: CGRect(x: x, y: y, width: w, height: h))
        ViewBorderRadius(bgView, Radius: 5, Width: 0.5, Color: UIColor.groupTableViewBackground)
        bgView.backgroundColor = UIColor.white
        contentView.addSubview(bgView)
        x = 10
        w = MCToolsManage.heightforString("电动自行车数量", andHeight: 44, fontSize: 14)
        y = 0
        h = 44
        var titleArray: [Any] = ["租车行名称", "租车行地址", "自行车数量", "电动自行车数量", "电动汽车数量"]
        var placeholderArray: [Any] = ["请输入租车行名称", "请输入租车行地址", "请输入自行车数量", "请输入电动自行车数量", "请输入电动汽车数量"]
        
        for i in 0..<titleArray.count {
            let str: String = titleArray[i] as! String
            let titleLbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
            titleLbl.textColor = UIColor.gray
            titleLbl.font = UIFont.systemFont(ofSize: CGFloat(14))
            bgView.addSubview(titleLbl)
            titleLbl.text = str
            let lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y + h), width: CGFloat(bgView.frame.width - 2 * x), height: CGFloat(0.5)))
            lineView.backgroundColor = UIColor.groupTableViewBackground
            bgView.addSubview(lineView)
            let Field = UITextField(frame: CGRect(x: CGFloat(x + w + 10), y: CGFloat(y), width: CGFloat(bgView.frame.width - (x + w + 10) - 10), height: CGFloat(44)))
            Field.placeholder = placeholderArray[i] as? String
            //@"请输入真实姓名";
            Field.font = UIFont.systemFont(ofSize: CGFloat(14))
            Field.tag = i + 200
            bgView.addSubview(Field)
            Field.textColor = UIColor.gray
            Field.text = textArray[i] as? String
            Field.delegate = viewController
            
            if i > 1 {
                Field.keyboardType = .numberPad
            }
            if i == 1 {
                Field.isUserInteractionEnabled = false
                seleAddBtn = UIButton(frame: CGRect(x: CGFloat(x + w + 10), y: CGFloat(y), width: CGFloat(bgView.frame.width - (x + w + 10) - 10), height: CGFloat(44)))
                bgView.addSubview(seleAddBtn!)
            }
            else {
                Field.isUserInteractionEnabled = true
            }
            
            if i + 1 < titleArray.count {
                y += h + 0.5
            }

        }

        
        
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
