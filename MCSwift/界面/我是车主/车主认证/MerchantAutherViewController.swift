//
//  MerchantAutherViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/27.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class MerchantAutherViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegateUINavigationControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate{
    var _tableView: UITableView!
    var alert: AlertViewExtension!
    var _addIdCardZ: UIButton!
    var _addIdCardF: UIButton!
    var _IdCardindex: Int = 0
    //1为正 2为反
    var img1: UIImage?
    var img2: UIImage?
    var nameStr: String = ""
    var addStr: String = ""
    var ZnumStr: String = ""
    var DnumStr: String = ""
    var DQnumStr: String = ""
    var textArray = [Any]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "车主认证"
        textArray = [Any]()
        
        
        textArray.append(nameStr)
        textArray.append(addStr )
        textArray.append(ZnumStr)
        textArray.append(DnumStr)
        textArray.append(DQnumStr)
        prepareUI()
        // Do any additional setup after loading the view.
    }
    func prepareUI() {
        _tableView = UITableView(frame: CGRect(x: CGFloat(0), y: CGFloat(64), width: CGFloat(Main_Screen_Width), height: CGFloat(Main_Screen_Height - 64)), style: .grouped)
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.separatorStyle = .none
        _tableView.backgroundColor = AppMCBgCOLOR
        view.addSubview(_tableView)
        preparefooerView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44 * 5 + 0.5 * 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid: String = "MerchantAutherTableViewCell"
        var cell: MerchantAutherTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellid) as! MerchantAutherTableViewCell?
        if cell == nil {
            cell = MerchantAutherTableViewCell(style: .default, reuseIdentifier: cellid)
            cell?.selectionStyle = .none
        }
        cell?.viewController = self
        
        cell?.textArray = textArray
        cell?.prepareUI()

        cell?.seleAddBtn?.addTarget(self, action: #selector(self.actionAddBtn), for: .touchUpInside)
        return cell!
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 200 {
            nameStr = textField.text!
        }
        else if textField.tag == 201 {
            addStr = textField.text!
        }
        else if textField.tag == 202 {
            ZnumStr = textField.text!
        }
        else if textField.tag == 203 {
            DnumStr = textField.text!
        }
        else if textField.tag == 204 {
            DQnumStr = textField.text!
        }
        
        textArray.removeAll()
        textArray.append(nameStr)
        textArray.append(addStr)
        textArray.append(ZnumStr)
        textArray.append(DnumStr)
        textArray.append(DQnumStr)
    }
    func preparefooerView(){
        
        var y: CGFloat = 0
        var x: CGFloat = 30
        var w: CGFloat = Main_Screen_Width - x * 2
        var h: CGFloat = MCAdaptiveH(TW: 625, TH: 253, VW: w)
        let vh: CGFloat = h * 2 + 15 + 5 + 20 + 30 + 40 + 40
        let bgView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(Main_Screen_Width), height: vh))
        //    bgView.backgroundColor =[UIColor whiteColor];
        _tableView.tableFooterView = bgView
        
        
        var idCardView = UIView(frame: CGRect(x: x, y: y, width: w, height: h))
//           idCardView.backgroundColor = UIColor.red;
        var borderLayer = CAShapeLayer()
        borderLayer.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(idCardView.frame.width), height: CGFloat(idCardView.frame.height))
        borderLayer.position = CGPoint(x: CGFloat(borderLayer.bounds.midX), y: CGFloat(borderLayer.bounds.midY))
        borderLayer.path = UIBezierPath(rect: borderLayer.bounds).cgPath
        borderLayer.lineWidth = 1.0 / UIScreen.main.scale
        
        //虚线边框
        borderLayer.lineDashPattern = [3, 3]
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.lightGray.cgColor
        idCardView.layer.addSublayer(borderLayer)
        bgView.addSubview(idCardView)

        
        
        var lw: CGFloat = MCToolsManage.heightforString("添加车行正面图(含店名)", andHeight: 20, fontSize: 14)
        var lx: CGFloat = (idCardView.frame.width - lw) / 2
        let ly: CGFloat = (idCardView.frame.height - 20) / 2
        var lbl = UILabel(frame: CGRect(x: lx, y: ly, width: lw, height: CGFloat(20)))
        lbl.text = "添加车行正面图(含店名)"
        lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
        lbl.textColor = UIColor.gray
        idCardView.addSubview(lbl)
        var addimgView = UIImageView(frame: CGRect(x: CGFloat(lx - 5 - 20), y: ly, width: CGFloat(20), height: CGFloat(20)))
        addimgView.image = UIImage(named: "card_add")
        idCardView.addSubview(addimgView)
        _addIdCardZ = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(idCardView.frame.width), height: CGFloat(idCardView.frame.height)))
        idCardView.addSubview(_addIdCardZ)
        _addIdCardZ.addTarget(self, action: #selector(self.actionAddidcard), for: .touchUpInside)
        y += h + 15
        idCardView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        //    idCardView.backgroundColor = [UIColor whiteColor];
        borderLayer = CAShapeLayer()
        borderLayer.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(idCardView.frame.width), height: CGFloat(idCardView.frame.height))
        borderLayer.position = CGPoint(x: CGFloat(borderLayer.bounds.midX), y: CGFloat(borderLayer.bounds.midY))
        borderLayer.path = UIBezierPath(rect: borderLayer.bounds).cgPath
        borderLayer.lineWidth = 1.0 / UIScreen.main.scale

        //虚线边框
        borderLayer.lineDashPattern = [3, 3]
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.lightGray.cgColor
        idCardView.layer.addSublayer(borderLayer)
        bgView.addSubview(idCardView)
        
        lw = MCToolsManage.heightforString("添加车行展示图片", andHeight: 20, fontSize: 14)
        lx = (idCardView.frame.width - lw) / 2
        //    CGFloat ly = (bgview.mj_h - 20)/2;
        lbl = UILabel(frame: CGRect(x: CGFloat(lx), y: CGFloat(ly), width: CGFloat(lw), height: CGFloat(20)))
        lbl.text = "添加车行展示图片"
        lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
        lbl.textColor = UIColor.gray
        idCardView.addSubview(lbl)
        addimgView = UIImageView(frame: CGRect(x: CGFloat(lx - 5 - 20), y: CGFloat(ly), width: CGFloat(20), height: CGFloat(20)))
        addimgView.image = UIImage(named: "card_add")
        idCardView.addSubview(addimgView)
        
        _addIdCardF = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(idCardView.frame.width), height: CGFloat(idCardView.frame.height)))
        idCardView.addSubview(_addIdCardF)
        _addIdCardF.addTarget(self, action: #selector(self.actionAddidcard), for: .touchUpInside)
        y = idCardView.frame.origin.y + idCardView.frame.height + 5
        h = 20
        x = idCardView.frame.origin.x
        w = Main_Screen_Width - x
        lbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lbl.text = "温馨提示:清晰可见的图片有助通过审核"
        lbl.font = UIFont.systemFont(ofSize: CGFloat(13))
        lbl.textColor = AppCOLOR
        bgView.addSubview(lbl)
        y += h + 30
        h = 40
        x = 30
        w = Main_Screen_Width - 2 * x
        var okBtn = UIButton(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        okBtn.backgroundColor = AppCOLOR
        okBtn.setTitle("提交", for: .normal)
        okBtn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(15))
        okBtn.setTitleColor(UIColor.white, for: .normal)
        ViewRadius(okBtn, Radius: 5)
        okBtn.addTarget(self, action: #selector(self.actionOKBtn), for: .touchUpInside)
        bgView.addSubview(okBtn)

    }
    func actionAddidcard(_ btn: UIButton) {
        if btn == _addIdCardZ {
            _IdCardindex = 1
        }
        else {
            _IdCardindex = 2
        }
        
        var actionSheet = HClActionSheet.init(title: "请选择", style: .default, itemTitles: ["拍照", "本地照片"])
        
        actionSheet?.delegate = self
        actionSheet?.tag = 200
        actionSheet?.itemTextColor = UIColor.black
        actionSheet?.cancleTextColor = UIColor.red
        //RGBCOLOR(36, 149, 221);
        actionSheet?.cancleTitle = "取消"
        
        actionSheet?.didFinishSelectIndex({ (_ index,title: String?) in
            print("block----\(Int(index))----\(title)")
            if index == 0 {
                //拍照
                self.PickerController()
            }
            else if index == 1 {
                self.btnClick()
            }
 
            
        })
    }
    
    func btnClick(){
        var picker = ZYQAssetPickerController()
        picker.maximumNumberOfSelection = 1
        picker.assetsFilter = ALAssetsFilter.allPhotos()
        picker.showEmptyGroups = false
        picker.delegate = self
        
        picker.selectionFilter = NSPredicate.init(block: { (_ evaluatedObject : Any,_ bindings:[AnyHashable: Any]) -> Bool in
            
            if (evaluatedObject as? ALAsset)?.value(forProperty: ALAssetPropertyType)?.isEqual(.video) {
                
                
                var duration: TimeInterval? = CDouble((evaluatedObject as? ALAsset)?.value(forProperty: ALAssetPropertyDuration))
                
                
                return duration >= 1
                
                
            }
            else {
                return true
            }
 
            
        })
        present(picker, animated: true, completion: { _ in })
        
    }
    func PickerController(){
        
    }
    func actionAddBtn() {
//        var ctl = MapSelectViewController()
//        pushNewViewController(ctl)
    }
    func actionOKBtn(){
        
        
    }
    func assetPickerController(_ picker: ZYQAssetPickerController, didFinishPickingAssets assets: [Any]) {
        print("\(assets.count)")
        
        
        
        for asset: ALAsset in assets as? NSArray {
            var tempImg = UIImage(cgImage: asset.defaultRepresentation().fullScreenImage())
            pickerimage(tempImg)
        }
    }
    
    func assetPickerControllerDidMaximum(_ picker: ZYQAssetPickerController) {
        print("到达上限")
    }
    func pickerimage(_ img: UIImage) {
        if _IdCardindex == 1 {
            _addIdCardZ.setBackgroundImage(img, for: 0)
            img1 = img
        }
        else {
            _addIdCardF.setBackgroundImage(img, for: 0)
            img2 = img
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
