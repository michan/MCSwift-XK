//
//  MyOwnerViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/22.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class MyOwnerViewController: BaseViewController {

    var _nametextField: UITextField!
    var _idCardtextField: UITextField!
    var _schooltextField: UITextField!
    var _personageBtn: UIButton!
    var _merchantBtn: UIButton!
    var _resBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI1()
        // Do any additional setup after loading the view.
    }
    
    //MARK:认证前
    func prepareUI1(){
        title = "成为车主"
        var x: CGFloat = 10
        var y: CGFloat = 64 + 30
        var w: CGFloat = Main_Screen_Width - 2 * x
        var h: CGFloat = 44 * 4 + 0.5 * 3
        let bgView = UIView(frame: CGRect(x: x, y: y, width: w, height: h))
        ViewBorderRadius(bgView, Radius: 5, Width: 0.5, Color: UIColor.groupTableViewBackground)
        bgView.backgroundColor = UIColor.white
        view.addSubview(bgView)
        x = 10
        w = MCToolsManage.heightforString("服务的学校", andHeight: 44, fontSize: 14)
        y = 0
        h = 44
        var titleArray: [Any] = ["真实姓名", "身份证号", "角色", "服务的学校"]

        for i in 0..<titleArray.count {
            let str: String = titleArray[i] as! String
            let titleLbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
            titleLbl.textColor = UIColor.gray
            titleLbl.font = UIFont.systemFont(ofSize: CGFloat(14))
            bgView.addSubview(titleLbl)
            titleLbl.text = str
            let lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y + h), width: CGFloat(bgView.frame.width - 2 * x), height: CGFloat(0.5)))
            lineView.backgroundColor = UIColor.groupTableViewBackground
            bgView.addSubview(lineView)
            if i == 0 {
                _nametextField = UITextField(frame: CGRect(x: CGFloat(x + w + 10), y: CGFloat(y), width: CGFloat(bgView.frame.width - (x + w + 10) - 10), height: CGFloat(44)))
                _nametextField.placeholder = "请输入真实姓名"
                _nametextField.font = UIFont.systemFont(ofSize: CGFloat(14))
                bgView.addSubview(_nametextField)
            }
            
          else if i == 1 {
                _idCardtextField = UITextField(frame: CGRect(x: CGFloat(x + w + 10), y: CGFloat(y), width: CGFloat(bgView.frame.width - (x + w + 10) - 10), height: CGFloat(44)))
                _idCardtextField.placeholder = "请输入身份证号"
                _idCardtextField.font = UIFont.systemFont(ofSize: CGFloat(14))
                bgView.addSubview(_idCardtextField)
            }
            
           else if i == 2 {
                _personageBtn = UIButton(frame: CGRect(x: CGFloat(x + w + 10), y: CGFloat(y), width: CGFloat(44), height: CGFloat(44)))
                _personageBtn.setImage(UIImage(named: "btn_chb"), for: .normal)
                _personageBtn.setImage(UIImage(named: "btn_chb_pre"), for: .selected)
                _personageBtn.addTarget(self, action: #selector(self.actionpersonageBtn), for: .touchUpInside)
                bgView.addSubview(_personageBtn)
                var lbl = UILabel(frame: CGRect(x: CGFloat(x + w + 10 + 44), y: CGFloat(y), width: CGFloat(64), height: CGFloat(44)))
                lbl.textColor = UIColor.gray
                lbl.text = "个人车主"
                lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
                bgView.addSubview(lbl)
                _merchantBtn = UIButton(frame: CGRect(x: CGFloat(lbl.frame.origin.x + lbl.frame.width), y: CGFloat(y), width: CGFloat(44), height: CGFloat(44)))
                _merchantBtn.setImage(UIImage(named: "btn_chb"), for: .normal)
                _merchantBtn.setImage(UIImage(named: "btn_chb_pre"), for: .selected)
                _merchantBtn.addTarget(self, action: #selector(self.actionmerchantBtn), for: .touchUpInside)
                bgView.addSubview(_merchantBtn)
                lbl = UILabel(frame: CGRect(x: CGFloat(_merchantBtn.frame.origin.x + 44), y: CGFloat(y), width: CGFloat(64), height: CGFloat(44)))
                lbl.textColor = UIColor.gray
                lbl.text = "租车行"
                lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
                bgView.addSubview(lbl)

            }
           else if i == 3 {
                _schooltextField = UITextField(frame: CGRect(x: CGFloat(x + w + 10), y: CGFloat(y), width: CGFloat(bgView.frame.width - (x + w + 10) - 10), height: CGFloat(44)))
                _schooltextField.placeholder = "请选择学校"
                _schooltextField.font = UIFont.systemFont(ofSize: CGFloat(14))
                _schooltextField.isUserInteractionEnabled = false
                bgView.addSubview(_schooltextField)
                let seleSchoolBtn = UIButton(frame: CGRect(x: CGFloat(x + w + 10), y: CGFloat(y), width: CGFloat(bgView.frame.width - (x + w + 10) - 10), height: CGFloat(44)))
                seleSchoolBtn.addTarget(self, action: #selector(self.actionseleSchoolBtn), for: .touchUpInside)
                bgView.addSubview(seleSchoolBtn)
            }
            if i + 1 < titleArray.count {
                y += h + 0.5
            }
 
            
        }
        x = bgView.frame.origin.x + 5
        y = bgView.frame.height + bgView.frame.origin.y + 8
        w = 30
        h = 30
        _resBtn = UIButton(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        _resBtn.setImage(UIImage(named: "btn_chb_square"), for: .normal)
        _resBtn.setImage(UIImage(named: "btn_chb_square_pre"), for: .selected)
        _resBtn.addTarget(self, action: #selector(self.action_resBtn), for: .touchUpInside)
        _resBtn.isSelected = true
        view.addSubview(_resBtn)
        x += w + 3
        w = Main_Screen_Width - x - 10
        let lbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lbl.text = "我已仔细查阅并同意《\(MCToolsManage.app_Name())用户协议》"
        lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
        lbl.textColor = UIColor.gray
        view.addSubview(lbl)
        w = MCToolsManage.heightforString(lbl.text, andHeight: 30, fontSize: 14)
        let btn = UIButton(frame: CGRect(x: CGFloat(x + 60), y: CGFloat(y), width: CGFloat(w - 60), height: CGFloat(30)))
        btn.addTarget(self, action: #selector(self.actionlookBtn), for: .touchUpInside)
        view.addSubview(btn)
        
        let okBtn = UIButton(frame: CGRect(x: CGFloat(30), y: CGFloat(y + 30 + 50), width: CGFloat(Main_Screen_Width - 2 * 30), height: CGFloat(40)))
        okBtn.backgroundColor = AppCOLOR
        okBtn.setTitle("下一步", for: .normal)
        okBtn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(15))
        okBtn.setTitleColor(UIColor.white, for: .normal)
        ViewRadius(okBtn, Radius: 5)
        okBtn.addTarget(self, action: #selector(self.actionOKBtn), for: .touchUpInside)
        view.addSubview(okBtn)

    }
    func actionOKBtn() {
        if _merchantBtn.isSelected {
            let ctl = MerchantAutherViewController()
            pushNewViewController(ctl)
        }
        else if _personageBtn.isSelected {
            let ctl = OwnerAuthenViewController()
            pushNewViewController(ctl)
        }
        else {
            showAllTextDialog("请选择角色")
            return
        }
        
    }
    func actionlookBtn() {
    }
    
    func action_resBtn(_ btn: UIButton) {
        btn.isSelected = !btn.isSelected
    }
    func actionpersonageBtn(_ btn: UIButton) {
        _merchantBtn.isSelected = false
        btn.isSelected = !btn.isSelected
    }
    func actionmerchantBtn(_ btn: UIButton) {
        _personageBtn.isSelected = false
        btn.isSelected = !btn.isSelected
    }
    
    func actionseleSchoolBtn() {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
