//
//  MCMapView.swift
//  MCSwift
//
//  Created by MC on 2017/3/23.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit





class MCMapView: UIView ,AMapLocationManagerDelegate,MAMapViewDelegate
{
    
    let defaultLocationTimeout = 6
    let defaultReGeocodeTimeout = 3

    
    var ViewController : BaseViewController!
    
    var  _ViewFrame : CGRect!
    
    
    var mapView : MAMapView!

    var completionBlock: AMapLocatingCompletionBlock!
    
    
   lazy var locationManager = AMapLocationManager()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
       _ViewFrame = frame
        MCMApManager.sharedInstance.updateLocation()
        initMapView()
        initCompleteBlock()
        configLocationManager()
        
        
        locationManager.requestLocation(withReGeocode: false, completionBlock: completionBlock)
        
        
    }
    
    func initCompleteBlock(){
        completionBlock = { [weak self] (location: CLLocation?, regeocode: AMapLocationReGeocode?, error: Error?) in
            
            if error != nil {
////                print("locError:{\(Int(error?.code)) - \(error?.localizedDescription)};")
//                NSLog("定位错误:{\(error.code) - \(error.localizedDescription)};")
//
//                //如果为定位失败的error，则不进行annotation的添加
//                if error?.code == .locateFailed {
//                    
//                    
//                    return
//                }
            }
            
            
            if let error = error {
                let error = error as NSError
                
                if error.code == AMapLocationErrorCode.locateFailed.rawValue {
                    //定位错误：此时location和regeocode没有返回值，不进行annotation的添加
                    NSLog("定位错误:{\(error.code) - \(error.localizedDescription)};")
                    
                    
                    return
                }
                else if error.code == AMapLocationErrorCode.reGeocodeFailed.rawValue
                    || error.code == AMapLocationErrorCode.timeOut.rawValue
                    || error.code == AMapLocationErrorCode.cannotFindHost.rawValue
                    || error.code == AMapLocationErrorCode.badURL.rawValue
                    || error.code == AMapLocationErrorCode.notConnectedToInternet.rawValue
                    || error.code == AMapLocationErrorCode.cannotConnectToHost.rawValue {
                    
                    //逆地理错误：在带逆地理的单次定位中，逆地理过程可能发生错误，此时location有返回值，regeocode无返回值，进行annotation的添加
                    NSLog("逆地理错误:{\(error.code) - \(error.localizedDescription)};")
                }
                else {
                    //没有错误：location有返回值，regeocode是否有返回值取决于是否进行逆地理操作，进行annotation的添加
                }
            }
            
            if (location != nil) {
                let annotation = MAPointAnnotation()
                
                
                annotation.coordinate = (location?.coordinate)!
                
                
                if (regeocode != nil) {
                    annotation.title = "\(regeocode?.formattedAddress)"
                    annotation.subtitle = "\(regeocode?.citycode)-\(regeocode?.adcode)-%.2fm"
                }
                else {
                    annotation.title = "lat:\(location?.coordinate.latitude);lon:\(location?.coordinate.longitude);"
                    annotation.subtitle = String(format: "accuracy:%.2fm", (location?.horizontalAccuracy)!)
                }
                
                self?.addAnnotation(toMapView: annotation)
            }
        }
        
        
        
        
    }

    func configLocationManager(){
        
        locationManager = AMapLocationManager()
        
        locationManager.delegate = self
        
        //设置期望定位精度
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        //设置不允许系统暂停定位
        self.locationManager.pausesLocationUpdatesAutomatically = false
        //设置允许在后台定位
        self.locationManager.allowsBackgroundLocationUpdates = true
        //设置定位超时时间
        self.locationManager.locationTimeout = defaultLocationTimeout
        //设置逆地理超时时间
        self.locationManager.reGeocodeTimeout = defaultReGeocodeTimeout
        
        
        
    }
    
    
    func initMapView (){
        
//        if mapView == nil {
        
            self.mapView = MAMapView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(_ViewFrame.size.width), height: CGFloat(_ViewFrame.size.height)))
            self.mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.mapView.zoomLevel = 17
            self.mapView.showsCompass = false
            self.mapView.showsScale = false
            ///如果您需要进入地图就显示定位小蓝点，则需要下面两行代码
            mapView.showsUserLocation = true
            
            mapView.userTrackingMode = .follow
           
            
            self.mapView.delegate = self
  
            self.addSubview(mapView)
            
            
            
//        }
        
        
    }
    
    func addAnnotation(toMapView annotation: MAAnnotation) {
        
        
        
        self.mapView.setZoomLevel(17.1, animated: false)
        self.mapView.setCenter(annotation.coordinate, animated: true)

        
        
    
    }

    func mapView(_ mapView: MAMapView!, viewFor annotation: MAAnnotation!) -> MAAnnotationView! {
        
        if (annotation is MAPointAnnotation) {
            let pointReuseIndetifier: String = "pointReuseIndetifier"
            var annotationView: MAPinAnnotationView? = (mapView.dequeueReusableAnnotationView(withIdentifier: pointReuseIndetifier) as? MAPinAnnotationView)
            if annotationView == nil {
                annotationView = MAPinAnnotationView(annotation: annotation, reuseIdentifier: pointReuseIndetifier)
            }
            annotationView?.canShowCallout = true
            annotationView?.animatesDrop = true
            annotationView?.isDraggable = false
            annotationView?.pinColor = .purple
            return annotationView!
        }
        return nil
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
//    var  locationManager : AMapLocationManager!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
