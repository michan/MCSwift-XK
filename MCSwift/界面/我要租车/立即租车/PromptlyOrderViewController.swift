//
//  PromptlyOrderViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/27.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class PromptlyOrderViewController: BaseViewController,CSItemSelectViewDelegate,MCSeleAlertViewDelegate,alertviewExtensionDelegate {
    
    
    var _typeLbl: UILabel!
    var timeBtn: UIButton!
    var dayBtn: UIButton!
    var timeselectView: CSItemSelectView!
    var dateselectView: CSItemSelectView!
    var lineImgView1: UIImageView!
    var lineImgView2: UIImageView!
    var lineImgView3: UIImageView!
    var lineImgView4: UIImageView!
    var priceLbl: UILabel!
    var _SeleAlertView: MCSeleAlertView!
    var _seleIndex: Int = 0
    //0自行车 1 电动自行 2 电动汽车
    var carStr: String = ""
    var _timeDateLbl: UILabel!
    var _gameTimer: Timer!
    var _gameStartTime: Date!
    var alert: AlertViewExtension!

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if _gameTimer != nil {
            _gameTimer.invalidate()
            _gameTimer.fire()

        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "立即租车"
        prepareUI()
        // Do any additional setup after loading the view.
    }

    func prepareUI(){
        var x: CGFloat = 10
        var y: CGFloat = 64
        var h: CGFloat = 44
        var w: CGFloat = 200
        var lbl = UILabel(frame: CGRect(x: x, y: y, width: w, height: h))
        lbl.textColor = UIColor.darkText
        lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
        lbl.text = "立即租车类型"
       self.view.addSubview(lbl)
        x = Main_Screen_Width - 10 - w
        _typeLbl = UILabel(frame: CGRect(x: x, y: y, width: w, height: h))
        _typeLbl.textColor = AppCOLOR
        _typeLbl.font = UIFont.systemFont(ofSize: CGFloat(14))
        _typeLbl.textAlignment = .right
        _typeLbl.text = "请选择"
        self.view.addSubview(_typeLbl)
        var btn = UIButton(frame: CGRect(x: CGFloat(Main_Screen_Width / 2), y: y, width: CGFloat(Main_Screen_Width / 2), height: h))
        btn.addTarget(self, action: #selector(self.actionType), for:.touchUpInside)
        self.view.addSubview(btn)
        
        
        x = 0
        y = 43.5 + 64
        h = 0.5
        w = Main_Screen_Width
        var lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lineView.backgroundColor = UIColor.lightGray
        self.view.addSubview(lineView)
        y += h + 20
        x = 10
        w = 30
        h = w
        timeBtn = UIButton(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        timeBtn.setImage(UIImage(named: "btn_chb"), for: .normal)
        timeBtn.setImage(UIImage(named: "btn_chb_pre"), for: .selected)
        timeBtn.isSelected = true
        timeBtn.addTarget(self, action: #selector(self.actionTimeBtn), for: .touchUpInside)
        self.view.addSubview(timeBtn)
        
        x += w + 5
        w = 200
        lbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lbl.textColor = UIColor.darkText
        lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
        lbl.text = "按小时"
        self.view.addSubview(lbl)
        x = 10
        y += h + 30
        h = 0.5
        w = Main_Screen_Width - 20
        lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lineView.backgroundColor = UIColor.lightGray
        self.view.addSubview(lineView)
        y += h
        h = 44
        var view1 = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        view1.backgroundColor = UIColor.lightGray
       self.view.addSubview(view1)
        y += h
        h = 0.5
        lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lineView.backgroundColor = UIColor.lightGray
        self.view.addSubview(lineView)
        timeselectView = CSItemSelectView.itemSelectView(withData: data1(), delegate: self, selectViewWidth: 50, defalutIndex: 3) as! CSItemSelectView!
        
        timeselectView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view1.frame.width), height: CGFloat(44))
        timeselectView.selectTitleColor = AppCOLOR
        //[UIColor greenColor];
        timeselectView.selectViewColor = UIColor.clear
        timeselectView.normalTitleColor = UIColor.gray
        timeselectView.titleFont = UIFont.systemFont(ofSize: CGFloat(18.0))
        timeselectView.backgroundColor = UIColor.groupTableViewBackground
         view1.addSubview(timeselectView)
        
        
        x = Main_Screen_Width / 2 - 25
        var ly: CGFloat = lineView.frame.origin.y - 44 - 0.5 - 30
        var lh: CGFloat = 44 + 60 + 1
        
        lineImgView1 = UIImageView(frame: CGRect(x: CGFloat(x), y: ly, width: CGFloat(10), height: lh))
        lineImgView1.image = UIImage(named: "cursor_left")
         self.view.addSubview(lineImgView1)
        x += 10 + 30
        lineImgView2 = UIImageView(frame: CGRect(x: CGFloat(x), y: ly, width: CGFloat(10), height: lh))
        lineImgView2.image = UIImage(named: "cursor_right")
         self.view.addSubview(lineImgView2)
        x = 10
        y = lineView.frame.origin.y + 0.5 + 40
        w = 30
        h = 30
        dayBtn = UIButton(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        dayBtn.setImage(UIImage(named: "btn_chb"), for: .normal)
        dayBtn.addTarget(self, action: #selector(self.actiondayBtn), for: .touchUpInside)
        dayBtn.setImage(UIImage(named: "btn_chb_pre"), for: .selected)
         self.view.addSubview(dayBtn)
        x += w + 5
        w = 200
        lbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lbl.textColor = UIColor.darkText
        lbl.font = UIFont.systemFont(ofSize: CGFloat(14))
        lbl.text = "按天"
         self.view.addSubview(lbl)
        x = 10
        y += h + 30
        h = 0.5
        w = Main_Screen_Width - 20
        lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lineView.backgroundColor = UIColor.lightGray
         self.view.addSubview(lineView)
        y += h
        h = 44
        view1 = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        view1.backgroundColor = UIColor.groupTableViewBackground
         self.view.addSubview(view1)
        y += h
        h = 0.5
        lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lineView.backgroundColor = UIColor.lightGray
         self.view.addSubview(lineView)
        dateselectView = CSItemSelectView.itemSelectView(withData: daydata1(), delegate: self, selectViewWidth: 50, defalutIndex: 3) as! CSItemSelectView!
            
            
        dateselectView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view1.frame.width), height: CGFloat(44))
        dateselectView.selectTitleColor = UIColor.gray
        //[UIColor greenColor];
        dateselectView.selectViewColor = UIColor.clear
        dateselectView.normalTitleColor = UIColor.gray
        dateselectView.titleFont = UIFont.systemFont(ofSize: CGFloat(18.0))
        dateselectView.backgroundColor = UIColor.groupTableViewBackground
         view1.addSubview(dateselectView)
        x = Main_Screen_Width / 2 - 25
        ly = lineView.frame.origin.y - 44 - 0.5 - 30
        lh = CGFloat(44 + 60 + 1)
        lineImgView3 = UIImageView(frame: CGRect(x: CGFloat(x), y: CGFloat(ly), width: CGFloat(10), height: CGFloat(lh)))
        lineImgView3.image = UIImage(named: "cursor_left")
        lineImgView3.isHidden = true
         self.view.addSubview(lineImgView3)
        x += 10 + 30
        lineImgView4 = UIImageView(frame: CGRect(x: CGFloat(x), y: CGFloat(ly), width: CGFloat(10), height: CGFloat(lh)))
        lineImgView4.image = UIImage(named: "cursor_right")
        lineImgView4.isHidden = true
         self.view.addSubview(lineImgView4)
        y = lineView.frame.origin.y + 0.5 + 30
        x = 0
        w = Main_Screen_Width
        h = 0.5
        lineView = UIView(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lineView.backgroundColor = UIColor.lightGray
         self.view.addSubview(lineView)
        y += h + 30
        x = 30
        w = Main_Screen_Width - 60
        h = 20
        lbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lbl.text = "预计租车价格"
        lbl.textColor = UIColor.gray
        lbl.font = UIFont.systemFont(ofSize: CGFloat(16))
        lbl.textAlignment = .center
         self.view.addSubview(lbl)
        y += h + 10
        priceLbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        priceLbl.text = "50.00元"
        priceLbl.textColor = UIColor.orange
        priceLbl.font = UIFont.systemFont(ofSize: CGFloat(16))
        priceLbl.textAlignment = .center
         self.view.addSubview(priceLbl)
        y += h + 20
        x = 30
        w = Main_Screen_Width - 60
        h = 40
        btn = UIButton(frame: CGRect(x: CGFloat(30), y: CGFloat(y), width: CGFloat(Main_Screen_Width - 60), height: CGFloat(40)))
        ViewRadius(btn, Radius: 5)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("去租车", for: .normal)
        btn.backgroundColor = AppCOLOR
        btn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(15))
         self.view.addSubview(btn)
        btn.addTarget(self, action: #selector(self.actionOK), for: .touchUpInside)

    }
    func actionOK(){
        prepareUI2()
    }
    func actionType(){
        
        _SeleAlertView = MCSeleAlertView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(Main_Screen_Width), height: CGFloat(Main_Screen_Height)), mCstyle: _seleIndex)
        _SeleAlertView.deldagate = self
        _SeleAlertView.showInWindow()
  
    }
    func mcSeleAlertViewHidden() {
        _SeleAlertView.removeFromSuperview()
    }
    
    func mcSeleAlertView(_ selectIndex: Int) {
        _seleIndex = selectIndex
        if _seleIndex == 0 {
            carStr = "自行车"
        }
        else if _seleIndex == 1 {
            carStr = "电动自行车"
        }
        else if _seleIndex == 2 {
            carStr = "电动汽车"
        }
        
        _SeleAlertView.removeFromSuperview()
        _typeLbl.text = carStr
    }
    
    func actionTimeBtn(){
        timeBtn.isSelected = true
        dayBtn.isSelected = false
        timeselectView.selectTitleColor = AppCOLOR
        //[UIColor grayColor];
        dateselectView.selectTitleColor = UIColor.gray
        lineImgView1.isHidden = false
        lineImgView2.isHidden = false
        lineImgView3.isHidden = true
        lineImgView4.isHidden = true
    }
    func actiondayBtn(){
        timeBtn.isSelected = false
        dayBtn.isSelected = true
        timeselectView.selectTitleColor = UIColor.gray
        dateselectView.selectTitleColor = AppCOLOR
        lineImgView1.isHidden = true
        lineImgView2.isHidden = true
        lineImgView3.isHidden = false
        lineImgView4.isHidden = false
    }
    func itemSelectView(_ itemView: CSItemSelectView, didSelectItem item: String, at index: Int) {
        print("item---\(item),index --- \(index)")
    }
    
    // MARK : 倒计时
    func prepareUI2(){
        
        self.title = "待抢单"
        
        let Bgimgview = UIImageView.init(frame: CGRect.init(x: 0, y: 64, width: Main_Screen_Width, height: Main_Screen_Height))
        Bgimgview.isUserInteractionEnabled = true
        Bgimgview.image = UIImage.init(named: "bg")
        self.view.addSubview(Bgimgview)
        
        var w : CGFloat = 250
        var h : CGFloat = w
        var y = 45 * MCHeightScale()
        var x = (Main_Screen_Width - w) / 2
        
        let view1 = UIView.init(frame: CGRect.init(x: x, y: y, width: w
            , height: h))
        ViewBorderRadius(view1, Radius: h / 2, Width: 15, Color: _appColor)
        
        Bgimgview.addSubview(view1)
        
        x += 15
        y += 15
        w -= 30
        h = w
        
        let view2 = UIView.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        ViewBorderRadius(view2, Radius: h / 2, Width: 5, Color: UIColor.white)
        Bgimgview.addSubview(view2)
        
        x = 5
        w = view2.frame.width - 10
        y = (h - 25 )/2
        h = 25
        
        
        _timeDateLbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        _timeDateLbl.text = "00:00:00"
        _timeDateLbl.textColor = UIColor.white
        _timeDateLbl.font = UIFont.systemFont(ofSize: CGFloat(30))
        _timeDateLbl.textAlignment = .center
        view2.addSubview(_timeDateLbl)
        
        
        y = view1.frame.origin.y + view1.frame.height + 30;
        w = Main_Screen_Width;
        h = 20;
        x = 0;
        
        let lbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: CGFloat(15))
        lbl.textAlignment = .center
        lbl.text = "请耐心等待，玩命叫车中"
        Bgimgview.addSubview(lbl)
        y += h + 70
        x = 30
        w = Main_Screen_Width - 60
        h = 40
        
        let btn = UIButton.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        ViewRadius(btn, Radius: 5)
        
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("取消订单", for: .normal)
        btn.backgroundColor = _appColor
        btn.addTarget(self, action: #selector(self.actionQXBtn), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(15))
        Bgimgview.addSubview(btn)
        _gameStartTime = Date()
        _gameTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        
        
        
        
    }
    
    func actionQXBtn(){
        
        alert = AlertViewExtension(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.frame.size.width), height: CGFloat(view.frame.size.height)))
        alert.delegate = self
        alert.setbackviewframeWidth(300, andheight: 150)
        alert.settipeTitleStr("确定要取消订单吗?", andfont: 14)
        alert._sureBtn.backgroundColor = UIColor.white
        alert._sureBtn.setTitleColor(UIColor.gray, for: .normal)
        alert._cancelBtn.setTitle("确定", for: .normal)
        alert._sureBtn.setTitle("取消", for: .normal)
        alert._sureBtn.setTitleColor(_appColor, for: .normal)
        alert._cancelBtn.backgroundColor = UIColor.white
        alert._cancelBtn.setTitleColor(UIColor.gray, for: .normal)
        view.addSubview(alert)
        
    }
    func clickBtnSelector(_ btn: UIButton) {
        alert.removeFromSuperview()
        if btn.tag == 2000 {
            //
            toPopVC()
            //取消
        }
        else {
            
        }
    }
    func updateTimer(_ sender : Timer){
        let deltaTime: Int = Int(sender.fireDate.timeIntervalSince(_gameStartTime!))
        let ss: String = getMMSSFromSS(deltaTime)
        _timeDateLbl.text = ss
        print("time ==\(ss)")
        
        
        
    }
    //传入 秒  得到 xx:xx:xx
    
    func getMMSSFromSS(_ seconds: Int) -> String {
        //    NSInteger seconds = [totalTime integerValue];
        //format of hour
        let str_hour = String(format: "%02ld", seconds / 3600)
        //format of minute
        let str_minute = String(format: "%02ld", (seconds % 3600) / 60)
        //format of second
        let str_second = String(format: "%02ld", seconds % 60)
        //format of time
        let format_time: String = "\(str_hour):\(str_minute):\(str_second)"
        return format_time
    }

    func data1() -> [Any] {
        var array = [Any]()
        for i in 0..<23 {
            let str: String = "\(i + 1)"
            array.append(str)
            
            
        }
        return array
    }
    
    func daydata1() -> [Any] {
        var array = [Any]()
        for i in 0..<29 {
            let str: String = "\(i + 1)"
            array.append(str)
        }
        return array
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
