//
//  SubscribeViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/24.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit




class SubscribeViewController: BaseViewController , UITableViewDelegate,UITableViewDataSource,MCSeleAlertViewDelegate,alertviewExtensionDelegate{

    var stateIndex = 0 //1待抢单
    var _tableView : UITableView!
    var _seleIndex = 0
    var carStr = ""
    var beginTimeStr : String?
    var seleTimeStr : String?
    var selectTimePicker : MHDatePicker?
    var _SeleAlertView : MCSeleAlertView?
    
    var  _timeDateLbl : UILabel!
    var  _gameTimer : Timer?
    var  _gameStartTime : Date!
    var alert :AlertViewExtension!
    
    override func viewWillDisappear(_ animated: Bool) {
        if _gameTimer  != nil {
         _gameTimer?.invalidate()
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "预约租车"
        prepareUI()
        // Do any additional setup after loading the view.
    }
    func prepareUI(){
        _tableView = UITableView.init(frame: CGRect.init(x: 0, y: 64, width: Main_Screen_Width, height: Main_Screen_Height - 64), style: .grouped)
        _tableView.delegate = self
        _tableView.dataSource = self
        self.view.addSubview(_tableView)
        
        prepareHeadView()
        prepareFooerView()
    }
    func prepareHeadView(){
        let imgview = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: Main_Screen_Width, height: MCAdaptiveH(TW: 740, TH: 375, VW: Main_Screen_Width)))
        imgview.image = UIImage.init(named: "photos")
        _tableView.tableHeaderView = imgview
        
        
        
    }
    func prepareFooerView(){
        let bgview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: Main_Screen_Width, height: 100))
        _tableView.tableFooterView = bgview
        let btn  = UIButton.init(frame: CGRect.init(x: 30, y: 100 - 40 - 20 - 5, width: Main_Screen_Width - 60, height: 40))
        ViewRadius(btn, Radius: 5)
        
        btn.setTitle("提交", for: .normal)
        btn.backgroundColor = AppCOLOR
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.addTarget(self, action: #selector(self.actionOK), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        bgview.addSubview(btn)
        
    }
    // MARK: 提交
    func actionOK(){
       
        prepareUI2()
        
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 44 * 2 + 1
        }
        return 44
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cellid = "MyTaxicabViewCell"
            var cell: MyTaxicabViewCell? = tableView.dequeueReusableCell(withIdentifier: cellid) as! MyTaxicabViewCell?
            
            if cell == nil {
                cell = Bundle.main.loadNibNamed(cellid, owner: self, options: nil)?.last as! MyTaxicabViewCell?
            }
            cell?.selectionStyle = .none
            
            cell?.titleLbl.text = "预约租车类型"
            cell?.subTitleLbl.text = "请选择"
            if (carStr.characters.count) > 0 {
                cell?.subTitleLbl.text = carStr

            }
            
            return cell!
        }
        else if indexPath.row == 1{
          
            let cellid2 = "WaitingOrderTableViewCell"
            var cell: WaitingOrderTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellid2) as! WaitingOrderTableViewCell?
            
            if cell == nil {
                cell = Bundle.main.loadNibNamed(cellid2, owner: self, options: nil)?.last as! WaitingOrderTableViewCell?
            }
            cell?.selectionStyle = .none
            cell?.beginTimeText.text = beginTimeStr
            cell?.seleTimeText.text = seleTimeStr
            cell?.seleTimeText.isUserInteractionEnabled = false
            cell?.beginTimeText.isUserInteractionEnabled = false
            cell?.btn1.addTarget(self, action: #selector(self.actionBtn1), for: .touchUpInside)
            cell?.btn2.addTarget(self, action: #selector(self.actionBtn2), for: .touchUpInside)
            
            return cell!

            
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            _SeleAlertView = MCSeleAlertView.init(frame: CGRect.init(x: 0, y: 0, width: Main_Screen_Width, height: Main_Screen_Height), mCstyle: _seleIndex)
            _SeleAlertView?.deldagate = self
            _SeleAlertView?.showInWindow()
            
        }
        
        
    }
    func mcSeleAlertView(_ selectIndex: Int) {
        _seleIndex = selectIndex
        if _seleIndex == 0 {
            carStr = "自行车"
        }
        else if _seleIndex == 1 {
            carStr = "电动自行车"

            
        }
        else if _seleIndex == 2 {
            carStr = "电动汽车"

            
        }
        
        _SeleAlertView?.removeFromSuperview()
        _tableView.reloadData()
    }
    
    //MARK: 选择时间
    
    func actionBtn1(){
        selectTimePicker = MHDatePicker()
        
        selectTimePicker?.didFinishSelectedDate({ (selectedDate : Date?) in
            
           self.beginTimeStr = self.dateString(with: selectedDate!, dateFormat: "YYYY年MM月dd日 HH:mm")
            self._tableView.reloadData()
        })
    
        
    }
    //MARK: 选择天

    func actionBtn2(){
        
       _SeleAlertView = MCSeleAlertView.init(frame: CGRect.init(x: 0, y: 0, width: Main_Screen_Width, height: Main_Screen_Height), mcDateTyle: 1, mCstyle: 2)
        _SeleAlertView?.deldagate = self
        _SeleAlertView?.showAnimateInWindow()
    }

    func mcSeleAlertViewHidden() {
        _SeleAlertView?.removeFromSuperview()
    }
    func mcSeleAlertView(_ selectDateIndex: Int, index: Int) {
        _SeleAlertView?.removeFromSuperview()
        var str = ""
        if selectDateIndex == 1 {
            str = "\(index)小时"
        }
        else
        {
            str = "\(index)天"
 
            
        }
        seleTimeStr = str;
        _tableView.reloadData()
    }
    func dateString(with date: Date, dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.locale = NSLocale(localeIdentifier: "zh_CN") as Locale!
        let str: String = dateFormatter.string(from: date)
        return str
    
    }
    
    
    
// MARK : 倒计时
    func prepareUI2(){
        
        self.title = "待抢单"
        _tableView.removeFromSuperview()
            let Bgimgview = UIImageView.init(frame: CGRect.init(x: 0, y: 64, width: Main_Screen_Width, height: Main_Screen_Height))
        Bgimgview.isUserInteractionEnabled = true
        Bgimgview.image = UIImage.init(named: "bg")
        self.view.addSubview(Bgimgview)
        
        var w : CGFloat = 250
        var h : CGFloat = w
        var y = 45 * MCHeightScale()
        var x = (Main_Screen_Width - w) / 2
        
        let view1 = UIView.init(frame: CGRect.init(x: x, y: y, width: w
            , height: h))
        ViewBorderRadius(view1, Radius: h / 2, Width: 15, Color: _appColor)
        
        Bgimgview.addSubview(view1)
        
        x += 15
        y += 15
        w -= 30
        h = w
        
        let view2 = UIView.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        ViewBorderRadius(view2, Radius: h / 2, Width: 5, Color: UIColor.white)
        Bgimgview.addSubview(view2)
        
        x = 5
        w = view2.frame.width - 10
        y = (h - 25 )/2
        h = 25
        
        
        _timeDateLbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        _timeDateLbl.text = "00:00:00"
        _timeDateLbl.textColor = UIColor.white
        _timeDateLbl.font = UIFont.systemFont(ofSize: CGFloat(30))
        _timeDateLbl.textAlignment = .center
        view2.addSubview(_timeDateLbl)

        
        y = view1.frame.origin.y + view1.frame.height + 30;
        w = Main_Screen_Width;
        h = 20;
        x = 0;

        let lbl = UILabel(frame: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(w), height: CGFloat(h)))
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: CGFloat(15))
        lbl.textAlignment = .center
        lbl.text = "请耐心等待，玩命叫车中"
        Bgimgview.addSubview(lbl)
        y += h + 70
        x = 30
        w = Main_Screen_Width - 60
        h = 40
        
        let btn = UIButton.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        ViewRadius(btn, Radius: 5)

        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("取消订单", for: .normal)
        btn.backgroundColor = _appColor
        btn.addTarget(self, action: #selector(self.actionQXBtn), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(15))
        Bgimgview.addSubview(btn)
        _gameStartTime = Date()
        _gameTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)

        
        
        
    }
    
    func actionQXBtn(){
        
        alert = AlertViewExtension(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.frame.size.width), height: CGFloat(view.frame.size.height)))
        alert.delegate = self
        alert.setbackviewframeWidth(300, andheight: 150)
        alert.settipeTitleStr("确定要取消订单吗?", andfont: 14)
        alert._sureBtn.backgroundColor = UIColor.white
        alert._sureBtn.setTitleColor(UIColor.gray, for: .normal)
        alert._cancelBtn.setTitle("确定", for: .normal)
        alert._sureBtn.setTitle("取消", for: .normal)
        alert._sureBtn.setTitleColor(_appColor, for: .normal)
        alert._cancelBtn.backgroundColor = UIColor.white
        alert._cancelBtn.setTitleColor(UIColor.gray, for: .normal)
        view.addSubview(alert)
        
    }
    func clickBtnSelector(_ btn: UIButton) {
        alert.removeFromSuperview()
        if btn.tag == 2000 {
            //
            toPopVC()
            //取消
        }
        else {
            
        }
    }
    func updateTimer(_ sender : Timer){
        let deltaTime: Int = Int(sender.fireDate.timeIntervalSince(_gameStartTime!))
        let ss: String = getMMSSFromSS(deltaTime)
        _timeDateLbl.text = ss
        print("time ==\(ss)")
   
        
        
    }
    //传入 秒  得到 xx:xx:xx
    
    func getMMSSFromSS(_ seconds: Int) -> String {
        //    NSInteger seconds = [totalTime integerValue];
        //format of hour
        let str_hour = String(format: "%02ld", seconds / 3600)
        //format of minute
        let str_minute = String(format: "%02ld", (seconds % 3600) / 60)
        //format of second
        let str_second = String(format: "%02ld", seconds % 60)
        //format of time
        let format_time: String = "\(str_hour):\(str_minute):\(str_second)"
        return format_time
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
