//
//  MyTaxicabViewCell.swift
//  MCSwift
//
//  Created by MC on 2017/3/24.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class MyTaxicabViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var subTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLbl.textColor = UIColor.gray
        subTitleLbl.textColor = _appColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
