//
//  WaitingOrderTableViewCell.swift
//  MCSwift
//
//  Created by MC on 2017/3/24.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class WaitingOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var beginTimeText: UITextField!
    
    @IBOutlet weak var seleTimeText: UITextField!
    
    @IBOutlet weak var btn1: UIButton!
    
    @IBOutlet weak var btn2: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
