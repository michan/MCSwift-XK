//
//  MyTaxicabViewController.swift
//  MCSwift
//
//  Created by MC on 2017/3/22.
//  Copyright © 2017年 MC. All rights reserved.
//

import UIKit

class MyTaxicabViewController: BaseViewController {

    var _MapView : MCMapView!
    var _messageView : UIView!
    var marquee: CHWMarqueeView?
    var _subscribeBtn : UIButton!//预约
    
    var _promptlyBtn : UIButton!//立即

    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addMapView()
        prepareUI()
        // Do any additional setup after loading the view.
    }
    func addMapView(){
        
        _MapView = MCMapView.init(frame: CGRect(x: CGFloat(0), y: CGFloat(64), width: CGFloat(Main_Screen_Width), height: CGFloat(Main_Screen_Height - 64 - 49)))
        _MapView.ViewController = self;
        self.view.addSubview(_MapView)
        
    }
    func prepareUI(){
       
        var x : CGFloat = 20
        var y : CGFloat = 64 + 10;
        var  w : CGFloat = Main_Screen_Width - 2 * x
        var h : CGFloat = 40
        
        _messageView = UIView.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        _messageView.backgroundColor = UIColor.white
        
        ViewBorderRadius(_messageView, Radius: h / 2, Width: 1, Color: _appColor)
        self.view.addSubview(_messageView)
        
        let string  = "购买会员租车更优惠哦，快点去购买吧"

        marquee = CHWMarqueeView(frame: CGRect.init(x: 35, y: 0, width: _messageView.frame.width - 35 - 10 - 20, height: 40), title: string)
        _messageView.addSubview(marquee!)

        x = 10
        y = 12.5
        w = 18
        h = 15
        let imgview = UIImageView.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        imgview.image = UIImage.init(named: "map_icon_notice")
        
        _messageView.addSubview(imgview)
        
        let lbl = UILabel.init(frame: CGRect.init(x: _messageView.frame.width - 10 - 20, y: 0, width: 20, height: 40))
        lbl.text = ">"
        lbl.textColor = UIColor.darkGray
        lbl.font = UIFont.systemFont(ofSize: 15)
        _messageView.addSubview(lbl)
        
        w = Main_Screen_Width / 4
        h = w
        x = w / 2
        y = Main_Screen_Height - 49 - 30 - h
        _subscribeBtn = UIButton.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        _subscribeBtn.setImage(UIImage.init(named: "map_btn_book"), for: .normal)
        
        _subscribeBtn.addTarget(self, action: #selector(self.actionSubBtn), for: .touchUpInside)
        self.view.addSubview(_subscribeBtn)
        
        x = Main_Screen_Width - w / 2 - w
        _promptlyBtn = UIButton.init(frame: CGRect.init(x: x, y: y, width: w, height: h))
        _promptlyBtn.setImage(UIImage.init(named: "map_btn_immediately"), for: .normal)
        
        _promptlyBtn.addTarget(self, action: #selector(self.actionpromptlyBtn), for: .touchUpInside)
        
        self.view.addSubview(_promptlyBtn)

        
        
    }
    func actionpromptlyBtn(){
     let ctl =  PromptlyOrderViewController()
        self.pushNewViewController(ctl)
        
    }
   
    
    func actionSubBtn(){
        
        let ctl = SubscribeViewController()
        self.pushNewViewController(ctl)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
